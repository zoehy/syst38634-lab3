package Time;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

//@RunWith(Parameterized.class)
public class TimeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
/*
	@Test
	public void testGetTotalSeconds() {
		int seconds = Time.getTotalSeconds(this.timetoTest);
		assertTrue("The seconds were not calculated properly", seconds == 43505);
	}
	
	@Test
	public void testGetTotalSecondsBoundaryAfter() {
		int seconds = Time.getTotalSeconds("13:05:05");
		assertTrue("The seconds were not calculated properly", seconds == 47105);
	}
	
	@Test
	public void testGetTotalSecondsBoundaryBefore() {
		int seconds = Time.getTotalSeconds("11:05:05");
		assertTrue("The seconds were not calculated properly", seconds == 39905);
	}
	
	@Test
	public void testGetTotalSecondsException() {
		int seconds = Time.getTotalSeconds("-12:05:05");
		assertTrue("The seconds were not calculated properly", seconds == -43505);
	} */
	
	
	@Test
	public void testGetMilliSeconds() {
		int milliSeconds = Time.getMilliSeconds("12:05:05:05");
		assertTrue("The Milliseconds were not calculated properly", milliSeconds == 5);
	}
	
	@Test ( expected = NumberFormatException.class )
	public void testGetMilliSecondsException() {
		int milliSeconds = Time.getMilliSeconds("12:05:05:0054");
		assertTrue("The Milliseconds were not calculated properly", milliSeconds == 5);
	}
	
	@Test
	public void testGetMilliSecondsBoundaryIn() {
		int milliSeconds = Time.getMilliSeconds("12:05:05:999");
		assertTrue("The Milliseconds were not calculated properly", milliSeconds == 999);
	}
	
	@Test ( expected = NumberFormatException.class )
	public void testGetMilliSecondsBoundaryOut() {
		int milliSeconds = Time.getMilliSeconds("12:05:05:1000");
		assertTrue("The Milliseconds were not calculated properly", milliSeconds == 0);
	}
	
/*

	@Test
	public void testGetSeconds() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalMinutes() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalHours() {
		fail("Not yet implemented");
	}
	
	public String timetoTest;
	
	public TimeTest(String dataTime) {
		timetoTest = dataTime;
	}
	
	@Parameterized.Parameters
	
	public static Collection<Object[]> loadData(){
		Object[][] data = {{"12:05:05"}};
		
		return Arrays.asList(data);
	}*/

}
