package Password;

public class PasswordValidator {
	public static void main(String[] args) {
		
	}
	
	public static boolean checkPasswordLength( String password ) {
		//Password must have at least 8 characters
		if(password.length() >= 8) {
			return true;
		}else {
			return false;
		}
	}
	
	public static boolean checkDigits( String password ) {
		//Initialize the counter
		int count = 0;
		
		//Count how many digits in the password
		for(int i=0; i < password.length(); i++) {
			if(Character.isDigit(password.charAt(i))) {
				count++;
			}
		}
		
		//Password must have at least 2 digits
		if(count >= 2) {
			return true;
		}else {
			return false;
		}
		
	} 
}

