package Password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testCheckPasswordLength() {
		boolean pwd = PasswordValidator.checkPasswordLength("12345678");
		assertTrue("The passwords were not properly", pwd == true);
	}
	
	@Test
	public void testCheckPasswordLengthException() {
		boolean pwd = PasswordValidator.checkPasswordLength("12");
		assertTrue("The passwords were not properly", pwd == false);
	}
	
	@Test
	public void testCheckPasswordLengthBoundaryIn() {
		boolean pwd = PasswordValidator.checkPasswordLength("87654321");
		assertTrue("The passwords were not properly", pwd == true);
	}
	
	@Test
	public void testCheckPasswordLengthBoundaryOut() {
		boolean pwd = PasswordValidator.checkPasswordLength("8765432");
		assertTrue("The passwords were not properly", pwd == false);
	}
 
	@Test
	public void testCheckDigits() {
		boolean digit = PasswordValidator.checkDigits("12abcdef");
		assertTrue("The digits were not properly", digit == true);
	}
	
	@Test 
	public void testCheckDigitsException() {
		boolean digit = PasswordValidator.checkDigits("abcdefgh");
		assertTrue("The digits were not properly", digit == false);
	}
	
	@Test
	public void testCheckDigitsBoundaryIn() {
		boolean digit = PasswordValidator.checkDigits("21fedcab");
		assertTrue("The digits were not properly", digit == true);
	} 
	
	@Test
	public void testCheckDigitsBoundaryOut() {
		boolean digit = PasswordValidator.checkDigits("1abcdefg");
		assertTrue("The digits were not properly", digit == false);
	}

}
