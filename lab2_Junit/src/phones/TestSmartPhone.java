package phones;

import static org.junit.Assert.*;

import java.text.DecimalFormat;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestSmartPhone {
	SmartPhone sp = new SmartPhone();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testGetFormattedPrice() {
		sp.setPrice(299.99);
		assertTrue("The price were not properly",sp.getFormattedPrice().equals("$299.99"));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryAfter() {
		sp.setPrice(9999.99);
		assertTrue("The price were not properly", sp.getFormattedPrice().equals("$9999.99"));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryBefore() {
		sp.setPrice(0);
		assertTrue("The price were not properly", sp.getFormattedPrice().equals("$0"));
	}
	
	@Test
	public void testGetFormattedPriceException() {
		sp.setPrice(-299.99);
		assertTrue("The price were not properly",sp.getFormattedPrice().equals("$299.99"));
	}
	
	@Test
	public void testSetVersion() throws VersionNumberException{
		sp.setVersion(3.0);
		assertTrue("The version were not properly", sp.getVersion() == 3.0);
	}
	
	@Test
	public void testSetVersionBoundaryAfter() throws VersionNumberException{
		sp.setVersion(4.0);
		assertTrue("The version were not properly", sp.getVersion() == 4.0);
	}
	
	@Test
	public void testSetVersionBoundaryBefore() throws VersionNumberException{
		sp.setVersion(0.0);
		assertTrue("The version were not properly", sp.getVersion() == 0.0);
	}
	
	@Test
	public void testSetVersionException() throws VersionNumberException{
		sp.setVersion(-3.0);
		assertTrue("The version were not properly", sp.getVersion() == 3.0);
	}

	
	
	
	
}
